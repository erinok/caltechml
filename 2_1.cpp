#include <cstdlib>
#include <ctime>
#include <cstdio>
using namespace std;

void experiment(double *v1, double *vrand, double *vmin) {
	int n1 = 0;
	int nRand = (random()/(RAND_MAX + 1.0))*1000;
	*vmin = 1;
	for(int i = 0; i < 1000; i++) {
		int nHeads = 0;
		for(int flip = 0; flip < 10; ++flip)
			nHeads += random() & 1;
		double v = nHeads/10.0;
		if(v < *vmin) *vmin = v;
		if(i == n1) *v1 = v;
		if(i == nRand) *vrand = v;
	}
}

struct OnlineMean {
	double mean; double n; OnlineMean() : mean(0), n(0) {}
	inline void record(double x) {mean = mean*(n/(n + 1.0)) + x/(n + 1.0); n += 1.0;}
};
	
int main() {
	srandom(time(0));
	OnlineMean v1, vrand, vmin;
	for(double i = 0; i < 100*1000; i++) {
		double a, b, c;
		experiment(&a, &b, &c);
		v1.record(a);
		vrand.record(b);
		vmin.record(c);
	}
	printf("v1 %f vrand %f vmin %f\n", v1.mean, vrand.mean, vmin.mean);
}

