#include <iostream>
#include <cstdlib>
#include <utility>
#include <armadillo>
using namespace std;
using namespace arma;

// return a 3-tuple row vector defining a line
vec randF() {
	vec f = zeros(3);
	double x0 = randu(), x1 = randu(), y0 = randu(), y1 = randu();
	f(0) = x0*y1 - x1*y0;
	f(1) = y0 - y1;
	f(2) = x1 - x0;
	return f;
}

double sign(double x) {
	if(x > 0) return 1;
	if(x < 0) return -1;
	return 0;
}

double eval(const mat& g, const mat& x) {
	return sign(dot(g,x));
}

// g is the pla seed
pair<double,vec> PLA(const mat& X, const mat& y, vec g) {
	double iters = 0;
	again:
	for(int i = 0; i < X.n_rows; i++) {
		double y_ = eval(g, X.row(i));
		if(y_ != y(i)) {
			g = g + (X.row(i)*y(i)).t();
			
			iters += 1;
			goto again;

			// cout << "g: " << g << endl;
			// cout << "y: " << y << endl;
			// cout << "ys[i]: " << ys[i] << endl;
		}
	}
	return make_pair(iters,g);
}

double error1000(vec f, vec g) {
	double neq = 0;
	int samples = 1000;
	vec x(3);
	x(0) = 1.0;
	for(int i = 0; i < samples; i++) {
		x(1) = randu();
		x(2) = randu();
		if(eval(f,x) != eval(g,x))
			neq += 1;
	}
	return neq/samples;
}

double sampleError(const mat& X, const mat& y, vec w) {
	double wrong = 0;
	for(int i = 0; i < X.n_rows; i++) 
		wrong += eval(w, X.row(i)) != y(i);
	return wrong/X.n_rows;
}

struct OnlineMean {
	double mean; double n;
	OnlineMean() : mean(0), n(0) {}
	inline void record(double x) {
		mean = mean*(n/(n + 1.0)) + x/(n + 1.0);
		n += 1.0;
	}
};

int main() {
	srand(time(0));
	int N = 100;
	mat X(N,3);
	mat y(N,1);
	
	OnlineMean Ein;	 // 2.5
	OnlineMean Eout; // 2.6
	OnlineMean PLAiters; // 2.7
	OnlineMean Eout_pla;
	for(double expmt = 0; ; expmt++) {
		mat f = randF();
		for(int i = 0; i < N; i++) {
			X(i,0) = 1.0;
			X(i,1) = randu();
			X(i,2) = randu();
			y(i) = eval(f,X.row(i));
		}
		vec w = pinv(X)*y;

		Ein.record(sampleError(X, y, w));
		Eout.record(error1000(f, w));
		
		w(0) = w(1) = w(2) = 0;
		pair<double,vec> pla = PLA(X, y, w);
		PLAiters.record(pla.first);
		Eout_pla.record(error1000(f, pla.second));

		if(int(expmt) % 1000 == 999) {
			cout << endl
			     << "Ein μ: " << Ein.mean << endl
			     << "Eout μ: " << Eout.mean << endl
			     << "PLAiters μ: " << PLAiters.mean << endl
			     << "Eout_pla μ: " << Eout_pla.mean << endl;
		}
	}
}
	

