#include <iostream>
#include <cstdlib>
#include <utility>
#include <armadillo>
using namespace std;
using namespace arma;

inline double sign(double x) { if(x > 0) return 1; if(x < 0) return -1; return 0; }

double target_f(double x1, double x2) { return sign(x1*x1 + x2*x2 - 0.6); }

void trainingSetWithNoise(mat &X, mat &y, int N=1000, double noisePercent=.1) {
	// XXX is this percent strategy OK?
	X = ones(N,3);
	y = zeros(N);
	for(int i = 0; i < N; i++) {
		X(i,1) = randu()*2 - 1;
		X(i,2) = randu()*2 - 1;
		y(i) = target_f(X(i,1),X(i,2));
		if(randu() < noisePercent)
			y(i) *= -1;
	}
}

mat transInput(const mat &X) {
	assert(X.n_cols == 3);
	mat XT(X.n_rows, 6);
	for(int i = 0; i < X.n_rows; i++) {
		XT(i,0) = 1;
		XT(i,1) = X(i,1);
		XT(i,2) = X(i,2);
		XT(i,3) = X(i,1)*X(i,2);
		XT(i,4) = X(i,1)*X(i,1);
		XT(i,5) = X(i,2)*X(i,2);
	}
	return XT;
}

double sampleError(const mat& X, const mat& y, vec w) {
	double wrong = 0;
	for(int i = 0; i < X.n_rows; i++) 
		wrong += sign(dot(w, X.row(i))) != y(i);
	return wrong/X.n_rows;
}

template<class X> struct OnlineMeanX {
	X mean; double n; OnlineMeanX(X init) : mean(init), n(0) {}
	inline void record(X x) { mean = mean*(n/(n + 1.0)) + x/(n + 1.0); }
};
struct OnlineMean {
	double mean; double n; OnlineMean() : mean(0), n(0) {}
	inline void record(double x) {mean = mean*(n/(n + 1.0)) + x/(n + 1.0); n += 1.0;}
};

int main() {
	srand(time(0));
	int N = 1000;
	OnlineMean Ein;	// 2.8
	OnlineMean Ein_trans; 
	OnlineMean agreements[5]; // 2.9
	OnlineMeanX<vec> avgWt(ones(6));
	OnlineMean Eout; // 2.10
	for(double expmt = 1; ; expmt++) {
		mat X, y;
		trainingSetWithNoise(X,y,N);
		vec w = pinv(X)*y;
		Ein.record(sampleError(X, y, w));

		mat XT = transInput(X);
		vec wt = pinv(XT)*y;
		Ein_trans.record(sampleError(XT,y,wt));
		avgWt.record(wt);

		// comparisons...
		double compare[5][6] = {{-1, -0.05, 0.08, 0.13, 1.51, 1.5}, {-1, -0.05, 0.08, 0.13, 1.51, 15}, {-1, -0.05, 0.08, 0.13, 151, 1.5}, {-1, -1.5, 0.08, 0.13, 0.051, 0.05}, {-1, -0.05, 0.08, 1.5, 0.151, 0.15}};
		for(int i = 0; i < 5; i++) {
			vec wt_(6); wt_(0) = compare[i][0]; wt_(1) = compare[i][1]; wt_(2) = compare[i][2]; wt_(3) = compare[i][3]; wt_(4) = compare[i][4]; wt_(5) = compare[i][5];
			OnlineMean ag;
			vec xt(6);
			for(int _ = 0; _ < 1000; _++) {
				double x1 = randu()*2 - 1; double x2 = randu()*2 - 1;
				xt(0) = 1; xt(1) = x1; xt(2) = x2; xt(3) = x1*x2; xt(4) = x1*x1; xt(5) = x2*x2;
				ag.record(sign(dot(wt,xt)) == sign(dot(wt_,xt)));
			}
			agreements[i].record(ag.mean);
		}

		mat X2, y2;
		trainingSetWithNoise(X2,y2,N);
		mat X2T = transInput(X2);
		Eout.record(sampleError(X2T, y2, wt));
		
		if(int(expmt) % 1000 == 0) {
			cout << endl
			     << "μ(Ein)       " << Ein.mean << endl
			     << "μ(Ein_trans) " << Ein_trans.mean << endl;
			for(int i = 0; i < 5; i++)
				cout << char('a' + i) << ": " << agreements[i].mean << endl;
			wt = avgWt.mean;
			printf("sign(%f %fx1 %fx2 %fx1x2 %fx1^2 + %fx2^2)\n", wt(0), wt(1), wt(2), wt(3), wt(4), wt(5));
			cout << "μ(Eout) " << Eout.mean << endl;
		}
	}
}
	

