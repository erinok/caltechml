#include <iostream>
#include <cmath>
using namespace std;

double E(double u, double v) { 
	double x = u*exp(v) - 2*v*exp(-u);
	return x*x;
}

double dE0(double u, double v) { 
	return 2*(exp(v) + 2*v*exp(-u))*(u*exp(v) - 2*v*exp(-u));
}

double dE1(double u, double v) {
	return 2*(-2*exp(-u) + exp(v)*u)*(exp(v)*u - 2*exp(-u)*v);
}

void gradDescent(double u, double v, double eta, double errCutoff) { 
	int iters = 0;
	while(1) { 
		double e = E(u,v);
		cout << e << "  " << u << "," << v << endl;
		if(e <= errCutoff) break;
		double nu = u - eta*dE0(u,v);
		double nv = v - eta*dE1(u,v);
		u = nu;
		v = nv;
		iters++;
	}
	cout << "final E: " << E(u,v) << endl;
	cout << "final u,v: " << u << "," << v << endl;
	cout << "iters: " << iters << endl;
}

int main() { 
	gradDescent(1,1, 0.1, 1e-14);
}
