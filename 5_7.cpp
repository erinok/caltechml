#include <iostream>
#include <cmath>
using namespace std;

double E(double u, double v) { 
	double x = u*exp(v) - 2*v*exp(-u);
	return x*x;
}

double dE0(double u, double v) { 
	return 2*(exp(v) + 2*v*exp(-u))*(u*exp(v) - 2*v*exp(-u));
}

double dE1(double u, double v) {
	return 2*(-2*exp(-u) + exp(v)*u)*(exp(v)*u - 2*exp(-u)*v);
}

void coordDescent(double u, double v, int iters, double eta) { 
	while(iters--) { 
		double e = E(u,v);
		cout << e << "  " << u << "," << v << endl;
		u = u - eta*dE0(u,v);
		v = v - eta*dE1(u,v);
	}
	cout << "final E: " << E(u,v) << endl;
	cout << "final u,v: " << u << "," << v << endl;
	cout << "iters: " << iters << endl;
}

int main() { 
	coordDescent(1,1, 15, .1);
}
