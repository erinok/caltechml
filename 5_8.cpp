#include <iostream>
#include <cstdlib>
#include <utility>
#include <cmath>
using namespace std;

double rnd() { return 1.0*rand()/(1.0 + RAND_MAX); }

struct v3 { double x[3]; };

v3 operator+(v3 a, v3 b) {
    v3 c = { a.x[0] + b.x[0], a.x[1] + b.x[1], a.x[2] + b.x[2] };
    return c;
}

double dot(v3 a, v3 b) {
    return a.x[0]*b.x[0] + a.x[1]*b.x[1] + a.x[2]*b.x[2];
}
        
v3 operator*(v3 a, double s) {
    v3 c = { a.x[0]*s, a.x[1]*s, a.x[2]*s };
    return c;
}
v3 operator*(double s, v3 a) { return a * s; }
v3 operator/(v3 a, double s) { return a*(1.0/s); }

v3 operator-(v3 a, v3 b) { return a + (b*-1.0); }

double mag(v3 v) { return sqrt(dot(v,v)); }

ostream& operator<<(ostream& os, v3 v) { return os << v.x[0] << ',' << v.x[1] << ',' << v.x[2]; }

// generate a random linear classifier on [0,1]^2 -- pass result eval() 
v3 randF() {
    double x0 = rnd(), x1 = rnd(), y0 = rnd(), y1 = rnd();
    v3 f = { x0*y1 - x1*y0, y0 - y1, x1 - x0 };
    return f;
}

double eval(v3 g, v3 x) {
    double p = dot(g,x);
    if(p < 0) return -1;
    if(p > 0) return 1;
    return 0;
}

// perceptron learning algorithm
pair<double,v3> PLA(int N, v3 xs[], double ys[]) {
    v3 g = {0,0,0};
    double iters = 0;

    again:
    for(int i = 0; i < N; i++) {
        double y = eval(g, xs[i]);
        if(y != ys[i]) {
            g = g + xs[i]*ys[i];
            iters += 1;
            goto again;
            
            // cout << "g: " << g << endl;
            // cout << "y: " << y << endl;
            // cout << "ys[i]: " << ys[i] << endl;
        }
    }
    return make_pair(iters,g);
}

// returns: (numEpochs,g) 
pair<double,v3> logisticRegressionStoachasticGradientDescent(double eta, int N, v3 xs[], double ys[]) { 
	v3 g = { 0,0,0 };
	double iters = 0;
	while(1) {
		v3 gPrev = g;
		for(int n = 0; n < N; n++) {
			//take a step based on data point n
			v3 v = (ys[n] * xs[n])/(1 + exp(ys[n] * dot(g, xs[n])));
			g = g + eta*v;
		}
		++iters;
		// new epoch termination criteria
		double delta = mag(g - gPrev);
		// cout << delta << ',';
		if(delta < .005) break;
	}
	// cout << endl;
	return make_pair(iters,g);
}
	

// return fraction(f[x] != g[x]) on 1000 randomly sampled x's
double error1000(v3 f, v3 g) {
    double neq = 0;
    int samples = 1000;
    for(int i = 0; i < samples; i++) {
        v3 x = { 1.0, rnd(), rnd() };
        if(eval(f,x) != eval(g,x))
            neq += 1;
    }
    return neq/samples;
}

// return cross entropy error betwwen g and f, assuming g is a logistic fn and f is a linear classifier (via eval)
double crossEntropyError(int samples, v3 g, v3 f) {
	double err = 0;
	for(double i = 0; i < samples; i++) { 
		v3 x = { 1.0, rnd(), rnd() };
		double y = eval(f,x);
		double e = log(1 + exp(-y * dot(g, x)));
		err = err*(i/(i + 1)) + e/(i + 1);
	}
	return err;
}

void test() { 
	const int N = 100;
	v3 xs[N] = {{1.0,0.143303, 0.139695}, {1.0,0.249455, 0.641672}, {1.0,0.438565, 0.885005},{1.0,0.686661, 0.367156}, {1.0,0.224382, 0.516378}, {1.0,0.74382, 0.17889}, {1.0,0.644681, 0.568105}, {1.0,0.518277, 0.98454}, {1.0,0.879324, 0.0495258}, {1.0,0.211393, 0.0151135}, {1.0,0.126563, 0.0709757}, {1.0,0.492447, 0.269286}, {1.0,0.838352, 0.0937948}, {1.0,0.610659, 0.311852}, {1.0,0.200433, 0.744355}, {1.0,0.178409, 0.372027}, {1.0,0.70582, 0.39939}, {1.0,0.495927, 0.0291267}, {1.0,0.830622, 0.185327}, {1.0,0.631087, 0.347897}, {1.0,0.434903, 0.284755}, {1.0,0.235112, 0.191518}, {1.0,0.310741, 0.0633513}, {1.0,0.451738, 0.505181}, {1.0,0.118557, 0.182768}, {1.0,0.417684, 0.938856}, {1.0,0.815525, 0.116814}, {1.0,0.442268, 0.210889}, {1.0,0.609104, 0.666392}, {1.0,0.194078, 0.576463}, {1.0,0.28018, 0.624283}, {1.0,0.736491, 0.542911}, {1.0,0.358478, 0.67623}, {1.0,0.656618, 0.361082}, {1.0,0.380211, 0.12733}, {1.0,0.609323, 0.867177}, {1.0,0.52673, 0.945484}, {1.0,0.880294, 0.940672}, {1.0,0.476872, 0.020091}, {1.0,0.00089757, 0.767275}, {1.0,0.514806, 0.359345}, {1.0,0.422582, 0.10718}, {1.0,0.228125, 0.0211687}, {1.0,0.097755, 0.536243}, {1.0,0.229613, 0.673863}, {1.0,0.99875, 0.0721015}, {1.0,0.789625, 0.609106}, {1.0,0.189306, 0.309475}, {1.0,0.424049, 0.64944}, {1.0,0.330612, 0.192245}, {1.0,0.962331, 0.789084}, {1.0,0.758258, 0.095287}, {1.0,0.321679, 0.753145}, {1.0,0.232558, 0.580505}, {1.0,0.601472, 0.0101829}, {1.0,0.849459, 0.00596143}, {1.0,0.806583, 0.843732}, {1.0,0.781395, 0.224399}, {1.0,0.467606, 0.797011}, {1.0,0.415949, 0.867427}, {1.0,0.456307, 0.0430124}, {1.0,0.486634, 0.965505}, {1.0,0.635836, 0.452938}, {1.0,0.626159, 0.132157}, {1.0,0.846056, 0.0171817}, {1.0,0.582939, 0.166885}, {1.0,0.657682, 0.907609}, {1.0,0.953649, 0.166773}, {1.0,0.752958, 0.507709}, {1.0,0.624384, 0.547811}, {1.0,0.838721, 0.190499}, {1.0,0.0331448, 0.207034}, {1.0,0.146522, 0.590329}, {1.0,0.423347, 0.386505}, {1.0,0.213235, 0.399301}, {1.0,0.808652, 0.340931}, {1.0,0.461723, 0.935419}, {1.0,0.963555, 0.0590677}, {1.0,0.422036, 0.96905}, {1.0,0.847851, 0.472693}, {1.0,0.183245, 0.323799}, {1.0,0.700047, 0.0715126}, {1.0,0.488317, 0.588861}, {1.0,0.0267969, 0.428443}, {1.0,0.239806, 0.0683419}, {1.0,0.62532, 0.693595}, {1.0,0.319947, 0.581}, {1.0,0.511825, 0.499772}, {1.0,0.156524, 0.73539}, {1.0,0.659098, 0.522879}, {1.0,0.791635, 0.0363692}, {1.0,0.782935, 0.238614}, {1.0,0.880498, 0.00960105}, {1.0,0.816678, 0.711881}, {1.0,0.579471, 0.931261}, {1.0,0.530602, 0.283199}, {1.0,0.794982, 0.957437}, {1.0,0.0077672, 0.471852}, {1.0,0.73896, 0.0724928}, {1.0,0.933615, 0.534445}};
	v3 f = {0.0574401, -0.174537, 0.211898};
	double ys[N]; for(int i = 0; i < N; i++) { ys[i] = eval(f,xs[i]); }
	pair<double,v3> res = logisticRegressionStoachasticGradientDescent(.1,N, xs, ys);
	cout << "iters: " << res.first << endl;
}


int main() {
	// test();
	// return 0;
	
    srand(time(0));
    double avgIters = 0;
    double avgEout = 0;
    for(double n = 0; n <= 100; n++) {
	    v3 f = randF();
        int N = 100;
        v3 xs[N];
        double ys[N];
        for(int i = 0; i < N; i++) {
            xs[i].x[0] = 1.0;
            xs[i].x[1] = rnd();
            xs[i].x[2] = rnd();
            ys[i] = eval(f,xs[i]);
		}
		
		// logistic regression
		{
			pair<double,v3> x = logisticRegressionStoachasticGradientDescent(.1, N, xs, ys);
			v3 g = x.second;
			double iters = x.first;
			double Eout = crossEntropyError(1000, g, f);
			avgEout = avgEout*(n/(n + 1)) + Eout/(n + 1);
			avgIters = avgIters*n/(n + 1) + iters/(n + 1);
		}

		// PLA	
		if(0) {
			pair<double,v3> iters_g = PLA(N, xs, ys);
			double iters = iters_g.first;
			v3 g = iters_g.second;
			double Eout = error1000(f,g);
		
			avgIters = avgIters*n/(n + 1) + iters/(n + 1);
			avgEout = avgEout*n/(n + 1) + Eout/(n + 1);

			if(int(n / 1000)*1000 == n)
				cout << endl
					 << "avg iters: " << avgIters << endl
					 << "avg Eout: " << avgEout << endl;
        }
    }
    cout << "avgEout: " << avgEout << endl;
    cout << "avgIters: " << avgIters << endl;
}

    
