#include <iostream>
#include <cstdlib>
#include <utility>
#include <armadillo>
using namespace std;
using namespace arma;

// return a 3-tuple row vector defining a line
vec randF() {
	vec f = zeros(3);
	double x0 = randu(), x1 = randu(), y0 = randu(), y1 = randu();
	f(0) = x0*y1 - x1*y0;
	f(1) = y0 - y1;
	f(2) = x1 - x0;
	return f;
}

double sign(double x) {
	if(x > 0) return 1;
	if(x < 0) return -1;
	return 0;
}

double eval(const mat& g, const mat& x) {
	return sign(dot(g,x));
}

double sampleError(const mat& X, const mat& y, vec w) {
	double wrong = 0;
	for(int i = 0; i < X.n_rows; i++) 
		wrong += eval(w, X.row(i)) != y(i);
	return wrong/X.n_rows;
}

struct OnlineMean {
	double mean; double n;
	OnlineMean() : mean(0), n(0) {}
	inline void record(double x) {
		mean = mean*(n/(n + 1.0)) + x/(n + 1.0);
		n += 1.0;
	}
};

mat transXtoZ(mat X) { 
	mat Z = ones<mat>(X.n_rows, 1); // transformed input
	Z = join_rows(Z, X);
	Z = join_rows(Z, X.col(0) % X.col(0));
	Z = join_rows(Z, X.col(1) % X.col(1));
	Z = join_rows(Z, X.col(0) % X.col(1));
	Z = join_rows(Z, sqrt(square((X.col(0) - X.col(1)))));
	Z = join_rows(Z, sqrt(square((X.col(0) + X.col(1)))));
	return Z;
}

int main() {
	// -- in and out sample data
	mat Din; Din.load("hw6/in.dta"); // in sample data
	mat Xin = Din.cols(0,1);
	mat Yin = Din.col(2);
	// cout << "Yin: " << endl << Yin << endl;
	// cout << "Xin: " << endl << Xin << endl;
	mat Zin = transXtoZ(Xin);
	
	mat Dout; Dout.load("hw6/out.dta");
	mat Xout = Dout.cols(0,1);
	mat Yout = Dout.col(2);
	mat Zout = transXtoZ(Xout);
	
	// -- normal linear regression
	mat wlin = (pinv(Zin.t()*Zin)*Zin.t())*Yin;
	cout << "wlin: " << wlin
		<< "error in: " << sampleError(Zin, Yin, wlin) << endl
		<< "error out: " << sampleError(Zout, Yout, wlin) << endl
		<< endl;
	
	// -- weight decay linear regression
	double bestErrOut = 1e1000;
	double bestErrOutK = 1e1000;
	
	for(double k = -5; k <= 5; k += 1) { 
		double lambda = pow(10,k);
		mat wreg = (pinv(Zin.t()*Zin + lambda*eye(8,8))*Zin.t())*Yin;
		double errOut = sampleError(Zout, Yout, wreg);
		cout // << "wreg: " << wreg
			<< "error in: " << sampleError(Zin, Yin, wreg) << endl
			<< "error out: " << errOut << endl
			<< "k: " << k << endl
			<< "lambda: " << lambda << endl
			<< endl;
		if(errOut < bestErrOut) {
			bestErrOut = errOut;
			bestErrOutK = k;
		}
	}
	cout << "bestErrOut: " << bestErrOut << endl
		<< "bestErrOutK: " << bestErrOutK << endl;
}
	
