
all: bin/6_2
	$^

bin/%: %.cpp | bin
	g++ -o $@ -O2 $< -larmadillo -framework Accelerate

bin:
	mkdir -p $@

clean:
	rm -rf bin

.PHONY: all clean
