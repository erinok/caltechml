import random

nblack, nwhite = 0, 0

while 1:
    bags = [[0,1,], [0,0]]
    random.shuffle(bags)
    random.shuffle(bags[0])
    if bags[0][0] == 0:
        if bags[0][1] == 0:
            nblack += 1
        else:
            nwhite += 1
    if (nblack + nwhite) % 1000 == 0:
        print nblack, nwhite, nblack / (nblack + nwhite + 0.0)
    
