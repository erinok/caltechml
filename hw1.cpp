#include <iostream>
#include <cstdlib>
#include <utility>
using namespace std;

double rnd() { return 1.0*rand()/(1.0 + RAND_MAX); }

struct v3 { double x[3]; };

v3 operator+(v3 a, v3 b) {
    v3 c = { a.x[0] + b.x[0], a.x[1] + b.x[1], a.x[2] + b.x[2] };
    return c;
}

double dot(v3 a, v3 b) {
    return a.x[0]*b.x[0] + a.x[1]*b.x[1] + a.x[2]*b.x[2];
}
        
v3 operator*(v3 a, double s) {
    v3 c = { a.x[0]*s, a.x[1]*s, a.x[2]*s };
    return c;
}

ostream& operator<<(ostream& os, v3 v) { return os << v.x[0] << ',' << v.x[1] << ',' << v.x[2]; }


v3 randF() {
    double x0 = rnd(), x1 = rnd(), y0 = rnd(), y1 = rnd();
    v3 f = { x0*y1 - x1*y0, y0 - y1, x1 - x0 };
    return f;
}

double eval(v3 g, v3 x) {
    double p = dot(g,x);
    if(p < 0) return -1;
    if(p > 0) return 1;
    return 0;
}

pair<double,v3> PLA(int N, v3 xs[], double ys[]) {
    v3 g = {0,0,0};
    double iters = 0;

    again:
    for(int i = 0; i < N; i++) {
        double y = eval(g, xs[i]);
        if(y != ys[i]) {
            g = g + xs[i]*ys[i];
            iters += 1;
            goto again;
            
            // cout << "g: " << g << endl;
            // cout << "y: " << y << endl;
            // cout << "ys[i]: " << ys[i] << endl;
        }
    }
    return make_pair(iters,g);
}

double error1000(v3 f, v3 g) {
    double neq = 0;
    int samples = 1000;
    for(int i = 0; i < samples; i++) {
        v3 x = { 1.0, rnd(), rnd() };
        if(eval(f,x) != eval(g,x))
            neq += 1;
    }
    return neq/samples;
}

int main() {
    srand(time(0));
    double avgIters = 0;
    double avgEout = 0;
    for(double n = 0; ; n++) {
        v3 f = randF();
        int N = 100;
        v3 xs[N];
        double ys[N];
        for(int i = 0; i < N; i++) {
            xs[i].x[0] = 1.0;
            xs[i].x[1] = rnd();
            xs[i].x[2] = rnd();
            ys[i] = eval(f,xs[i]);
        }
        pair<double,v3> iters_g = PLA(N, xs, ys);
        double iters = iters_g.first;
        v3 g = iters_g.second;
        double Eout = error1000(f,g);
        
        avgIters = avgIters*n/(n + 1) + iters/(n + 1);
        avgEout = avgEout*n/(n + 1) + Eout/(n + 1);

        if(int(n / 1000)*1000 == n)
            cout << endl
                 << "avg iters: " << avgIters << endl
                 << "avg Eout: " << avgEout << endl;
    }
}

    
