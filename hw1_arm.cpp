#include <iostream>
#include <cstdlib>
#include <utility>
#include <armadillo>
using namespace std;
using namespace arma;

// return a 3-tuple row vector defining a line
vec randF() {
	vec f = zeros(3);
	double x0 = randu(), x1 = randu(), y0 = randu(), y1 = randu();
	f(0) = x0*y1 - x1*y0;
	f(1) = y0 - y1;
	f(2) = x1 - x0;
	return f;
}

double eval(const vec& g, const vec& x) {
    double p = dot(g,x);
    if(p < 0) return -1;
    if(p > 0) return 1;
    return 0;
}

pair<double,vec> PLA(const vector<vec>& xs, double ys[]) {
    vec g = zeros(3);
    double iters = 0;

    for(int i = 0; i < xs.size(); i++) {
        double y = eval(g, xs[i]);
        if(y != ys[i]) {
            g = g + xs[i]*ys[i];
            iters += 1;
            i = 0;

            // cout << "g: " << g << endl;
            // cout << "y: " << y << endl;
            // cout << "ys[i]: " << ys[i] << endl;
        }
    }
    return make_pair(iters,g);
}

double samplePne(vec f, vec g) {
    double neq = 0;
    int samples = 1000;
    vec x(3);
    x(0) = 1.0;
    for(int i = 0; i < samples; i++) {
	    x(1) = randu();
	    x(2) = randu();
        if(eval(f,x) != eval(g,x))
            neq += 1;
    }
    return neq/samples;
}

int main() {
    srand(time(0));
    
    double avgIters = 0;
    double avgPne = 0;
    for(double n = 0; ; n++) {
        vec f = randF();
        int N = 100;
        vector<vec> xs(N);
        double ys[N];
        for(int i = 0; i < N; i++) {
	        xs[i] << 1.0 << randu() << randu();
            ys[i] = eval(f,xs[i]);
        }
        pair<double,vec> iters_g = PLA(xs, ys);
        double iters = iters_g.first;
        vec g = iters_g.second;
        double Pne = samplePne(f,g);
        
        avgIters = avgIters*n/(n + 1) + iters/(n + 1);
        avgPne = avgPne*n/(n + 1) + Pne/(n + 1);

        if(int(n / 1000)*1000 == n)
            cout << endl
                 << avgIters << endl
                 << avgPne << endl;
    }
}
