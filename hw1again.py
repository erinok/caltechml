from scipy import *
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

# return a random percepton target function on [0,1]**2
def randF():
    x0,y0,x1,y1 = random.random(4)
    return array([x0*y1 - x1*y0, y0 - y1, x1 - x0])

def evalP(g, x):
    return sign(dot(g,x))

def PLA(xs,ys):
    g = array([0.0,0.0,0.0])
    iters = 0
    while 1:
        ok = True
        for i in xrange(len(ys)):
            y = sign(dot(g,xs[i]))
            if y != ys[i]:
                g = g + ys[i]*xs[i]
                iters += 1
                ok = False
        if ok:
            break
    return iters

def rnd():
    return random.random(1)[0]

def ex7():
    iters = []
    N = 100
    for _ in range(1000):
        f = randF()
        xs = array([[1.0,rnd(),rnd()] for _ in range(N)])
        ys = array([sign(dot(xs[i],f)) for i in range(N)])
        iters.append(PLA(xs,ys))

    print 1.0*sum(iters)/len(iters)
    
