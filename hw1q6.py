N = 3

possibleTargetFns = range(2**N)

def scoreHypothesis(g):
    score = 0
    for f in possibleTargetFns:
        numAgree = 0
        for inp in range(N):
            gVal = g & (1 << inp)
            fVal = f & (1 << inp)
            numAgree += gVal == fVal
        if numAgree == 3:
            score += 3
        elif numAgree == 2:
            score += 2
        elif numAgree == 1:
            score += 1
        elif numAgree == 0:
            score += 0
        else: assert False
    return score

# inputs: '101', '110', '111'
#           4      2      1
# gC        0      0      1
# gD        1      1      0

gA = 7
gB = 0
gC = 1
gD = 6

print 'gA', scoreHypothesis(gA)
print 'gB', scoreHypothesis(gB)
print 'gC', scoreHypothesis(gC)
print 'gD', scoreHypothesis(gD)

# they are all equiv... is this obvious? should we intuitively know this? feels that way but can't quite pinpoint it

