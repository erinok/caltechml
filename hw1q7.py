import random
rnd = random.random

def vadd(x,y):
    assert len(x) == len(y)
    z = [0.0]*len(x)
    for i in xrange(len(x)):
        z[i] = x[i] + y[i]
    return z

def vscale(s,v):
    z = [0.0]*len(v)
    for i in xrange(len(v)):
        z[i] = v[i]*s
    return z

def vdot(x,y):
    assert len(x) == len(y)
    z = 0.0
    for i in xrange(len(x)):
        z += x[i]*y[i]
    return z

def sign(x):
    if x >= 0: return 1
    if x < 0: return -1
    return 0

# return a random percepton target function on [0,1]**2
def randF():
    x0,y0,x1,y1 = rnd(),rnd(),rnd(),rnd()
    return [x0*y1 - x1*y0, y0 - y1, x1 - x0]

def evalP(g, x):
    return sign(vdot(g, x))

# run the perceptron learning algorithm to find a hypothesis that matches the given data points. retuns the number of iterations until convergence. starts w/ a 0 percepton.
def PLA(xs,target_ys):
    g = [0,0,0]
    iters = 0
    while 1:
        for x,target_y in zip(xs,target_ys):
            y = evalP(g,x)
            if y != target_y:
                g = vadd(g, vscale(target_y, x))
                iters += 1
                # if iters % 1000 == 0:
                #     print
                #     print 'y', y
                #     print 'x', x
                #     print 'scaled', vscale(y, x)
                #     print 'g', g
                #     print 'iters', iters, g
                #     print
                break
        else:
            # print iters
            # we are done
            return iters

def ex7():
    iters = []
    N = 100
    for _ in range(1000):
        f = randF()
        xs = [[1.0,rnd(),rnd()] for _ in range(N)]
        ys = [evalP(f,x) for x in xs]
        iters.append(PLA(xs,ys))

    print 1.0*sum(iters)/len(iters)
    
